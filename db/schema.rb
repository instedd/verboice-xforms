# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120708041452) do

  create_table "calls", :force => true do |t|
    t.integer  "form_id"
    t.string   "phone"
    t.text     "data"
    t.string   "status"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "verboice_call_id"
    t.datetime "not_before"
    t.binary   "session"
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "endpoints", :force => true do |t|
    t.string   "name"
    t.string   "guid"
    t.string   "callback_url"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
    t.string   "schedule"
    t.string   "time_zone"
    t.string   "channel"
    t.integer  "retry_count",  :default => 3
  end

  create_table "forms", :force => true do |t|
    t.string   "name"
    t.text     "xml",         :limit => 16777215
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.text     "mappings"
    t.integer  "endpoint_id"
    t.string   "external_id"
    t.datetime "file_date"
    t.string   "file_name"
  end

end
