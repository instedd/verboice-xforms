class AddRetryCountToEndpoint < ActiveRecord::Migration
  def change
    add_column :endpoints, :retry_count, :integer, :default => 3
  end
end
