class ChangeFormXmlToBinary < ActiveRecord::Migration
  def up
    change_column :forms, :xml, :text, :limit => 16777215
  end

  def down
    change_column :forms, :xml, :text
  end
end
