class AddChannelToEndpoint < ActiveRecord::Migration
  def change
    add_column :endpoints, :channel, :string
  end
end
