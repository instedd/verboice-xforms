class AddTimeZoneToEndpoint < ActiveRecord::Migration
  def change
    add_column :endpoints, :time_zone, :string
  end
end