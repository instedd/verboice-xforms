class CreateCalls < ActiveRecord::Migration
  def change
    create_table :calls do |t|
      t.integer :form_id
      t.integer :endpoint_id
      t.string :phone
      t.text :data
      t.string :status

      t.timestamps
    end
  end
end
