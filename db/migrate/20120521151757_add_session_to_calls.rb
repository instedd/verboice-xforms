class AddSessionToCalls < ActiveRecord::Migration
  def change
    add_column :calls, :session, :binary
  end
end
