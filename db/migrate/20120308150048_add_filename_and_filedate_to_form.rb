class AddFilenameAndFiledateToForm < ActiveRecord::Migration
  def change
    add_column :forms, :file_date, :datetime
    add_column :forms, :file_name, :string
  end
end
