class AddEndpointIdToForm < ActiveRecord::Migration
  def change
    add_column :forms, :endpoint_id, :integer
  end
end
