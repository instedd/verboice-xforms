class RenameEndpointQueueToSchedule < ActiveRecord::Migration
  def change
  	rename_column :endpoints, :queue, :schedule
  end
end
