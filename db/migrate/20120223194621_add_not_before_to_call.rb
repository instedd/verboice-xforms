class AddNotBeforeToCall < ActiveRecord::Migration
  def change
    add_column :calls, :not_before, :datetime
  end
end
