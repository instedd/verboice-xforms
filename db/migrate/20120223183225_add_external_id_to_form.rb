class AddExternalIdToForm < ActiveRecord::Migration
  def change
    add_column :forms, :external_id, :string
  end
end
