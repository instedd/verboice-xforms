class RemoveEndpointIdFromCall < ActiveRecord::Migration
  def change
    remove_column :calls, :endpoint_id
  end
end
