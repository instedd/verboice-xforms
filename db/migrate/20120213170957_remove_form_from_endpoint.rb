class RemoveFormFromEndpoint < ActiveRecord::Migration
  def change
    remove_index :endpoints, :form_id
    change_table :endpoints do |t|
      t.remove :form_id
    end
  end
end
