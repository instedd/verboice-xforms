class AddQueueToEndpoint < ActiveRecord::Migration
  def change
    add_column :endpoints, :queue, :string
  end
end
