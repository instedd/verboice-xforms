class CreateEndpoints < ActiveRecord::Migration
  def change
    create_table :endpoints do |t|
      t.belongs_to :form
      t.string :name
      t.string :guid
      t.string :callback_url

      t.timestamps
    end
    add_index :endpoints, :form_id
  end
end
