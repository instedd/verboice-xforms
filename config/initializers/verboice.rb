class Verboice
  attr_accessor :project_id

  def self.instance
    @verboice ||= load_verboice_config
  end

  def self.load_verboice_config
    config = YAML.load_file(File.expand_path 'config/verboice.yml', Rails.root)
    client = Verboice.new config['url'], config['account'], config['password'], config['channel']
    client.project_id = config['project']
    client
  end

  def project_schedules
    schedules(project_id)
  end
end
