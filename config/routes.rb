VerboiceXForms::Application.routes.draw do

  resources :forms do
    resources :recordings, :controller => :form_recordings do
      member do
        post '/', :action => :save
      end
    end
    member do
      get 'xml'
    end
  end
  resources :endpoints do
    resources :recordings, :controller => :endpoint_recordings do
      member do
        post '/', :action => :save
      end
    end
  end
  resources :calls

  match '/:guid' => 'endpoints#enqueue', :guid => /[0-9a-f]{32}/, :as => 'enqueue'

  scope '/verboice' do
    match '/callback'
    match '/status'
  end

  match '/' => redirect('/forms')

  get 'terms_and_conditions', :to => redirect('/')
end
