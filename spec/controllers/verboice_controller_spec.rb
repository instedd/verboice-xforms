require 'spec_helper'

describe VerboiceController do

  let(:call_id) { Random.rand.to_s }
  before(:each) do
    Verboice.instance.stub(:call) { {'call_id' => call_id, 'status' => 'queued'} }
    Call.create! :form_id => form.id, :phone => '555', :verboice_call_id => call_id
  end

  context 'two inputs' do
    let(:form) do
      Form.make :xml => <<-END
        <xform xmlns:xf="http://www.w3.org/2002/xforms">
          <xf:model>
            <xf:instance>
              <data>
                <input1/><input2/>
              </data>
            </xf:instance>
          </xf:model>
          <xf:input ref='data/input1'>
            <xf:label>Hello</xf:label>
          </xf:input>
          <xf:input ref='data/input2'>
            <xf:label>Good Bye</xf:label>
          </xf:input>
        </xform>
      END
    end
    after(:each) { FileUtils.rm_rf "#{Rails.root}/data/endpoint/#{form.endpoint.id}" }

    it 'gather input field' do
      get :callback, :CallSid => call_id
      response.body.should be_equivalent_to("<Response><Gather><Say>Hello</Say></Gather><Redirect>#{callback_url}</Redirect></Response>")
      read_session[:control].should == 0
      read_session[:model_instance].should be_equivalent_to("<data><input1/><input2/></data>")
    end

    it 'answer to first input' do
      write_session :control => 0
      write_session :model_instance => '<data><input1/><input2/></data>'
      get :callback, :CallSid => call_id, :Digits => '1234'
      response.body.should be_equivalent_to("<Response><Pause/><Gather><Say>Good Bye</Say></Gather><Redirect>#{callback_url}</Redirect></Response>")
      read_session[:control].should == 1
      read_session[:model_instance].should be_equivalent_to("<data><input1>1234</input1><input2/></data>")
    end

    it 'answer to first input with confirmation message' do
      form.endpoint.save_recording 'confirmation_message', StringIO.new('1234')
      write_session :control => 0
      write_session :model_instance => '<data><input1/><input2/></data>'
      get :callback, :CallSid => call_id, :Digits => '1234'
      response.body.should be_equivalent_to("<Response><Pause/><Play>#{endpoint_recording_url(form.endpoint, 'confirmation_message')}</Play><Gather><Say>Good Bye</Say></Gather><Redirect>#{callback_url}</Redirect></Response>")
      read_session[:control].should == 1
      read_session[:model_instance].should be_equivalent_to("<data><input1>1234</input1><input2/></data>")
    end

    it 'answer second input' do
      write_session :control => 1
      write_session :model_instance => '<data><input1>1234</input1><input2/></data>'
      get :callback, :CallSid => call_id, :Digits => '4567'
      response.body.should be_equivalent_to("<Response><Pause/><Hangup/></Response>")
      read_session[:model_instance].should be_equivalent_to("<data><input1>1234</input1><input2>4567</input2></data>")
    end

    it 'retry first input' do
      write_session :control => 0
      write_session :model_instance => '<data><input1/><input2>qqq</input2></data>'
      get :callback, :CallSid => call_id
      response.body.should be_equivalent_to("<Response><Gather><Say>Hello</Say></Gather><Redirect>#{callback_url}</Redirect></Response>")
      read_session[:control].should == 0
      read_session[:retry].should == 1
      read_session[:model_instance].should be_equivalent_to("<data><input1/><input2>qqq</input2></data>")
    end

    it 'retry first input second time' do
      write_session :control => 0
      write_session :model_instance => '<data><input1/><input2>qqq</input2></data>'
      write_session :retry => 1
      get :callback, :CallSid => call_id
      response.body.should be_equivalent_to("<Response><Gather><Say>Hello</Say></Gather><Redirect>#{callback_url}</Redirect></Response>")
      read_session[:control].should == 0
      read_session[:retry].should == 2
      read_session[:model_instance].should be_equivalent_to("<data><input1/><input2>qqq</input2></data>")
    end

    it 'retry first input third time' do
      write_session :control => 0
      write_session :model_instance => '<data><input1/><input2>qqq</input2></data>'
      write_session :retry => 2
      get :callback, :CallSid => call_id
      response.body.should be_equivalent_to("<Response><Hangup/></Response>")
    end

    it 'retry first input third time and play error message before hangup' do
      form.endpoint.save_recording 'error_message', StringIO.new('1234')
      write_session :control => 0
      write_session :model_instance => '<data><input1/><input2>qqq</input2></data>'
      write_session :retry => 2
      get :callback, :CallSid => call_id
      response.body.should be_equivalent_to("<Response><Play>#{endpoint_recording_url(form.endpoint, 'error_message')}</Play><Hangup/></Response>")
    end

    it 'allow number of retries specified at endpoint' do
      form.endpoint.retry_count = 2
      form.endpoint.save!
      write_session :control => 0
      write_session :model_instance => '<data><input1/><input2>qqq</input2></data>'
      write_session :retry => 1
      get :callback, :CallSid => call_id
      response.body.should be_equivalent_to("<Response><Hangup/></Response>")
    end
  end

  context 'select1' do
    let(:form) do
      Form.make :xml => <<-END
        <xform xmlns:xf="http://www.w3.org/2002/xforms">
          <xf:model>
            <xf:instance>
              <data />
            </xf:instance>
          </xf:model>
          <xf:select1 ref="data">
            <xf:label>Select one</xf:label>
            <xf:item>
              <xf:label>First</xf:label>
              <xf:value>A</xf:value>
            </xf:item>
            <xf:item>
              <xf:label>Second</xf:label>
              <xf:value>B</xf:value>
            </xf:item>
          </xf:select1>
        </xform>
      END
    end

    it "gathers select1" do
      get :callback, :CallSid => call_id
      response.body.should be_equivalent_to("<Response><Gather numDigits='1' timeout='0'><Say>Select one</Say></Gather><Gather numDigits='1' timeout='0'><Say>#{I18n.t :press_number_for_text, :number => 1, :text => 'First'}</Say></Gather><Gather numDigits='1'><Say>#{I18n.t :press_number_for_text, :number => 2, :text => 'Second'}</Say></Gather><Redirect>#{callback_url}</Redirect></Response>")
    end

    it "receives select1 response" do
      write_session :control => 0
      write_session :model_instance => '<data/>'
      get :callback, :CallSid => call_id, :Digits => '2'
      response.body.should be_equivalent_to("<Response><Pause/><Hangup/></Response>")
      read_session[:model_instance].should be_equivalent_to("<data>B</data>")
    end

    it "receives select1 response out of range" do
      write_session :control => 0
      write_session :model_instance => '<data>C</data>'
      get :callback, :CallSid => call_id, :Digits => '3'
      response.body.should be_equivalent_to("<Response><Pause/><Gather numDigits='1' timeout='0'><Say>Select one</Say></Gather><Gather numDigits='1' timeout='0'><Say>#{I18n.t :press_number_for_text, :number => 1, :text => 'First'}</Say></Gather><Gather numDigits='1'><Say>#{I18n.t :press_number_for_text, :number => 2, :text => 'Second'}</Say></Gather><Redirect>#{callback_url}</Redirect></Response>")
      read_session[:control].should == 0
      read_session[:retry].should == 1
      read_session[:model_instance].should be_equivalent_to("<data>C</data>")
    end

    it 'retry select third time' do
      write_session :control => 0
      write_session :model_instance => '<data/>'
      write_session :retry => 2
      get :callback, :CallSid => call_id, :Digits => '3'
      response.body.should be_equivalent_to("<Response><Pause/><Hangup/></Response>")
    end

    it 'retry select third time and reply with error message before hangup' do
      form.endpoint.save_recording 'error_message', StringIO.new('1234')
      write_session :control => 0
      write_session :model_instance => '<data/>'
      write_session :retry => 2
      get :callback, :CallSid => call_id, :Digits => '3'
      response.body.should be_equivalent_to("<Response><Pause/><Play>#{endpoint_recording_url(form.endpoint, 'error_message')}</Play><Hangup/></Response>")
    end
  end

  context 'read only input' do
    let(:form) do
      Form.make :xml => <<-END
        <xform xmlns:xf="http://www.w3.org/2002/xforms">
          <xf:model>
            <xf:instance>
              <data>Hello</data>
            </xf:instance>
            <xf:bind id='bind1' readonly='true()' nodeset='data' />
          </xf:model>
          <xf:input bind='bind1'>
            <xf:label>Label</xf:label>
          </xf:input>
        </xform>
      END
    end

    it "gathers select1" do
      get :callback, :CallSid => call_id
      response.body.should be_equivalent_to("<Response><Say>Label</Say><Say>Hello</Say><Redirect>#{callback_url}</Redirect></Response>")
    end

    it "hangups after first field" do
      write_session :control => 0
      write_session :model_instance => form.xform.model_instance.to_s
      get :callback, :CallSid => call_id
      response.body.should be_equivalent_to("<Response><Hangup/></Response>")
    end
  end

  context 'validation' do
    let(:form) do
      Form.make :xml => <<-END
        <xform xmlns:xf="http://www.w3.org/2002/xforms">
          <xf:model>
            <xf:instance>
              <data>Hello</data>
            </xf:instance>
            <xf:bind id='bind1' constraint='. = 10' nodeset='data' />
          </xf:model>
          <xf:input bind='bind1'>
            <xf:label>Label</xf:label>
          </xf:input>
        </xform>
      END
    end

    it "reject invalid value" do
      write_session :control => 0
      write_session :model_instance => form.xform.model_instance.to_s
      get :callback, :CallSid => call_id, :Digits => '33'
      response.body.should be_equivalent_to("<Response><Pause/><Gather><Say>Label</Say></Gather><Redirect>#{callback_url}</Redirect></Response>")
    end

    it "accepts valid valid" do
      write_session :control => 0
      write_session :model_instance => form.xform.model_instance.to_s
      get :callback, :CallSid => call_id, :Digits => '10'
      response.body.should be_equivalent_to("<Response><Pause/><Hangup/></Response>")
    end
  end

  context 'validation with message' do
    let(:form) do
      Form.make :xml => <<-END
        <xform xmlns:xf="http://www.w3.org/2002/xforms" xmlns:jr='http://openrosa.org/javarosa'>
          <xf:model>
            <xf:instance>
              <data>Hello</data>
            </xf:instance>
            <xf:bind id='bind1' constraint='. = 10' nodeset='data' jr:constraintMsg='Error' />
          </xf:model>
          <xf:input bind='bind1'>
            <xf:label>Label</xf:label>
          </xf:input>
        </xform>
      END
    end

    it "reject invalid value with message" do
      write_session :control => 0
      write_session :model_instance => form.xform.model_instance.to_s
      get :callback, :CallSid => call_id, :Digits => '33'
      response.body.should be_equivalent_to("<Response><Pause/><Say>Error</Say><Gather><Say>Label</Say></Gather><Redirect>#{callback_url}</Redirect></Response>")
    end
  end

  context 'uses relevant property' do
    let(:form) do
      Form.make :xml => <<-END
        <xform xmlns:xf="http://www.w3.org/2002/xforms">
          <xf:model>
            <xf:instance>
              <data></data>
            </xf:instance>
            <xf:bind id='bind1' relevant='data = 10' nodeset='data' />
          </xf:model>
          <xf:input ref="data">
            <xf:label>Label1</xf:label>
          </xf:input>
          <xf:input bind="bind1">
            <xf:label>Label2</xf:label>
          </xf:input>
        </xform>
      END
    end

    it "skips irrelevant field" do
      write_session :control => 0
      write_session :model_instance => form.xform.model_instance.to_s
      get :callback, :CallSid => call_id, :Digits => '5'
      response.body.should be_equivalent_to("<Response><Pause/><Hangup/></Response>")
    end

    it "shows relevant field" do
      write_session :control => 0
      write_session :model_instance => form.xform.model_instance.to_s
      get :callback, :CallSid => call_id, :Digits => '10'
      response.body.should be_equivalent_to("<Response><Pause/><Gather><Say>Label2</Say></Gather><Redirect>#{callback_url}</Redirect></Response>")
    end
  end

  context 'call with data' do
    let(:form) do
      Form.make :xml => <<-END
        <xform xmlns:xf="http://www.w3.org/2002/xforms">
          <xf:model>
            <xf:instance>
              <data></data>
            </xf:instance>
          </xf:model>
          <xf:input ref="data">
            <xf:label>Label1</xf:label>
          </xf:input>
        </xform>
      END
    end
    before(:each) do
      Call.find_by_verboice_call_id(call_id).update_attributes! :data => '<data>123</data>'
    end

    it "uses the initial data set" do
      get :callback, :CallSid => call_id
      read_session[:model_instance].should be_equivalent_to("<data>123</data>")
    end
  end

  context 'finishing' do
    let(:form) do
      Form.make :xml => <<-END
        <xform xmlns:xf="http://www.w3.org/2002/xforms">
          <xf:model>
            <xf:instance>
              <data>foo</data>
            </xf:instance>
          </xf:model>
        </xform>
      END
    end

    it "finishes call" do
      call = double("call", :data => nil, :form => form, :session => nil)
      Call.stub(:find_by_verboice_call_id).and_return(call)
      call.should_receive(:update_attributes!)
      call.should_receive(:finish).with(be_equivalent_to("<data>foo</data>"))
      get :callback, :CallSid => call_id
    end
  end

  def read_session
    Call.find_by_verboice_call_id(call_id).session || {}
  end

  def write_session values
    call = Call.find_by_verboice_call_id(call_id)
    session = call.session || {}
    session.merge! values
    call.update_attributes! :session => session
  end



end