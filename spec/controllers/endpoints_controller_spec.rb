require 'spec_helper'

describe EndpointsController do
  let(:endpoint) { Endpoint.make :time_zone => "Buenos Aires" }
  let(:form) do
    mappings = {'value1' => '/data/value1', 'value2' => '/data/value2'}
    Form.make :mappings => mappings, :endpoint => endpoint, :xml => <<-END
      <xform xmlns:xf="http://www.w3.org/2002/xforms">
        <xf:model>
          <xf:instance>
            <data><value1/><value2/></data>
          </xf:instance>
        </xf:model>
      </xform>
    END
  end
  let(:phone) { Faker::PhoneNumber.phone_number }
  let(:queued_call) { Call.last }
  let(:verboice_call_id) { Random.rand.to_s }

  it 'enqueues call using guid url' do
    Verboice.instance.should_receive(:call).with(phone, {:queue => endpoint.queue, :channel => endpoint.channel}).and_return({'call_id' => verboice_call_id, 'state' => 'queued'})
    get :enqueue, :guid => endpoint.guid, :phone_number => phone, :form_id => form.external_id, :value1 => 'foo', :value2 => 'bar'
    queued_call.phone.should == phone
    queued_call.form.should == form
    queued_call.data.should be_equivalent_to('<data><value1>foo</value1><value2>bar</value2></data>')
  end

  it 'enqueues call parsing time' do
    call_date = "27-02-2012"
    call_time = "6:30:00 PM"

    Verboice.instance.stub(:call).and_return({})

    get :enqueue, :guid => endpoint.guid, :phone_number => phone, :form_id => form.external_id, :call_date => call_date, :call_time => call_time

    queued_call.not_before.should eq(Time.utc(2012, 2, 27, 21, 30))
  end
end