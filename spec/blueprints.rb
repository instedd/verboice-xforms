require 'machinist/active_record'
require 'sham'
require 'faker'

Sham.name  { Faker::Name.name }
Sham.callback_url { "http://#{Faker::Internet.domain_name}" }

Form.blueprint do
  name
  endpoint
  external_id { (rand * 1000).to_i }
end

Endpoint.blueprint do
  name
  queue { Sham.name }
  channel { Sham.name }
  guid { Digest::MD5.hexdigest Time.now.to_s }
  callback_url
end
