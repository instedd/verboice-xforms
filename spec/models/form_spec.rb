require 'spec_helper'

describe Form do
  let(:xml) do
    <<-END
      <xform xmlns:xf="http://www.w3.org/2002/xforms">
        <xf:model>
          <xf:instance>
            <data />
          </xf:instance>
        </xf:model>
      </xform>
    END
  end
  let(:form) { Form.new :name => "form", :xml => xml, :endpoint => Endpoint.make }

  it "load xform from xml" do
    form.xform.model_instance.root.name.should == 'data'
  end

  it "xform is cached" do
    form.xform.should equal(form.xform)
  end

  it "can store large forms" do
    form.xml = '*' * 256 * 1024
    form.save!
    form.reload
    form.xml.length.should == 256 * 1024
  end

  context "mappings" do
    before(:each) do
      form.mappings['data'] = '/data'
      form.save!
    end

    it "stores mappings" do
      form2 = Form.find_by_id(form.id)
      form2.mappings.should == {'data' => '/data'}
    end

    it "maps from xml element" do
      xml = Nokogiri::XML("<data>123</data>")
      data = form.map_from_xml(xml)
      data.should == {'data' => '123'}
    end

    it "maps from xml attribute" do
      xml = Nokogiri::XML("<data value='123'/>")
      form.mappings['data'] = '/data/@value'
      data = form.map_from_xml(xml)
      data.should == {'data' => '123'}
    end

    it "maps from xml given as string" do
      xml = "<data value='123'/>"
      form.mappings['data'] = '/data/@value'
      data = form.map_from_xml(xml)
      data.should == {'data' => '123'}
    end

    it "maps to null if node not found" do
      xml = Nokogiri::XML("<data>123</data>")
      form.mappings['data'] = '/data/@value'
      data = form.map_from_xml(xml)
      data.should == {'data' => nil}
    end

    it "apply map to XML" do
      xml = Nokogiri::XML("<data/>")
      form.map_to_xml(xml, {'data' => 123})
      xml.should be_equivalent_to('<data>123</data>')
    end

    it "apply map to XML attribute" do
      xml = Nokogiri::XML("<data value=''/>")
      form.mappings['data'] = '/data/@value'
      form.map_to_xml(xml, {'data' => 123})
      xml.should be_equivalent_to('<data value="123" />')
    end
  end

  context "recordings" do
    before(:each) { form.save! }
    after(:each) { FileUtils.rm_rf "#{Rails.root}/data/form/#{form.id}" }

    it "saves recording" do
      form.save_recording 'blabla'.md5, StringIO.new("\xB0")
      File.exists?("#{Rails.root}/data/form/#{form.id}/#{'blabla'.md5}.wav").should == true
    end

    it "returns nil if recording does not exist" do
      form.get_recording('blabla'.md5).should be_nil
    end

    it "returns path for existing recording" do
      form.save_recording 'blabla'.md5, StringIO.new('1234')
      form.get_recording('blabla'.md5).should == "#{Rails.root}/data/form/#{form.id}/#{'blabla'.md5}.wav"
    end

    it "deletes recording" do
      form.save_recording 'blabla'.md5, StringIO.new('1234')
      form.delete_recording 'blabla'.md5
      form.get_recording('blabla'.md5).should be_nil
    end
  end

end