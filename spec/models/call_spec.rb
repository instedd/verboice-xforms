require 'spec_helper'

describe Call do
  let(:xml) do
    <<-END
      <xform xmlns:xf="http://www.w3.org/2002/xforms">
        <xf:model>
          <xf:instance>
            <data />
          </xf:instance>
        </xf:model>
      </xform>
    END
  end
  let(:form) { Form.make :xml => xml }

  it "enqueue in verboice on save" do
    Verboice.instance.should_receive(:call).with('123123', anything).and_return({'call_id' => '888', 'state' => 'queued'})
    call = Call.create :phone => '123123', :form => form
    call.verboice_call_id.should == '888'
    call.status.should == 'queued'
  end

  it "saves mapped fields" do
    Verboice.instance.stub(:call) { {'call_id' => '888', 'status' => 'queued'} }
    form.mappings['value'] = '/data'
    call = Call.create :phone => '123123', :form => form, :fields => {'value' => '999'}
    call.data.should be_equivalent_to('<data>999</data>')
  end

  it "enqueue in verboice using endpoint queue and channel" do
    Verboice.instance.should_receive(:call).with('123123', {:queue => form.endpoint.queue, :channel => form.endpoint.channel}).and_return({})
    Call.create :phone => '123123', :form => form
  end

  it "enqueues in verboice using not_before date" do
    not_before = Time.now
    Verboice.instance.should_receive(:call).with('123123', {:queue => anything, :channel => anything, :not_before => not_before}).and_return({})
    Call.create :phone => '123123', :form => form, :not_before => not_before
  end

  it "finishes saving data and posting results to endpoint" do
    Verboice.instance.stub(:call).and_return({})
    call = Call.create :phone => '123123', :form => form
    call.stub(:endpoint).and_return(form.endpoint)
    form.endpoint.should_receive(:post_results).with(call)
    call.finish "new xml data"
    call.reload.data.should eq("new xml data")
  end
end
