require 'spec_helper'

describe Endpoint do
  before :all do
    Delayed::Worker.delay_jobs = false
  end
  
  it "generates guid before save" do
    endpoint = Endpoint.make
    endpoint.guid.should match(/[0-9a-f]{32}/)
  end

  it "uses given guid" do
    guid = Digest::MD5.hexdigest 'foo'
    endpoint = Endpoint.make :guid => guid
    endpoint.guid.should == guid
  end
  
  it "posts results" do
    endpoint = Endpoint.make
    call = double("call", :data => "xml data")
    RestClient.should_receive(:post).with(endpoint.callback_url, {:xml_submission_file => call.data, :multipart => true})    
    endpoint.post_results call
  end
end
