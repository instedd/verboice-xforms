module ApplicationHelper
  
  def fdate date
    if date
      date.strftime "%b %d %Y %H:%M"
    else
      nil
    end
  end
  
end
