var mappings;

$(document).ready(function() {
  mappings = $("#mappings");
  set_up_template();
  set_up_add_mapping();
  set_up_submit();
  
  unpack($("#form_mappings_str").val());
});

function unpack(mappings) {
  mappings = $.parseJSON(mappings);
  $.each(mappings, function(k, v) {
      var mapping = create_new_mapping();
      fill_mapping(mapping, k, v);
  });
}

function pack() {
  var mappings = {};
  var mappings_input = $("#form_mappings_str");
  var mappings_list = $("#mappings");
  $("li", mappings_list).not(":hidden").not("#add_mapping").each(function(i, element){
    mappings[$("span", element).text()] = $("input", element).val();
  });
  mappings_input.val(JSON.stringify(mappings));
}

function add_mapping(event) {
  var mapping = create_new_mapping();
  var key = $("#add_mapping_key");
  var value = $("#add_mapping_value");
  
  fill_mapping(mapping, key.val(), value.val());
  
  key.val('');
  value.val('');
  
  event.preventDefault();
}

function fill_mapping(mapping, key, value) {
  $("span", mapping).text(key);
  $("input", mapping).val(value);
}

function create_new_mapping() {
  var mapping = $("#mapping_template").clone(true);
  mapping.removeAttr("id");
  mapping.show();
  mapping.insertBefore($("#add_mapping"));
  return mapping;
}

function set_up_template() {
  var template = $("#mapping_template");
  template.hide();
  $(".mclist-remove", template).click(function(e){
    e.preventDefault();
    $(this).parent().detach();
  });
}

function set_up_add_mapping() {
  var add_mapping_button = $(".mclist-add", mappings);
  add_mapping_button.click(add_mapping);
}

function set_up_submit() {
  $("#submit_form").click(function(e) {
    e.preventDefault();
    pack();
    $(this).parents('form').submit();
  });
}