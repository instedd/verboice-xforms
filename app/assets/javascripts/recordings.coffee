#= require 'recorder'
#= require 'jquery.ui.widget'
#= require 'jquery.fileupload'


$ ->
  setupGUI = ->

  Wami.setup
    id: "wami"
    swfUrl: "/Wami.swf"
    onReady: setupGUI

  $(".fileupload").fileupload
    type: 'POST'

  $(".fplay").click (e) ->
    e.preventDefault()
    url = $(this).closest("[data-path]").data("path")
    Wami.startPlaying url

  $(".frecording").live "click", (e) ->
    e.preventDefault()
    $(this).removeClass("frecording").addClass "fstop"
    url = $(this).closest("[data-path]").data("path")
    Wami.startRecording url

  $(".fstop").live "click", (e) ->
    e.preventDefault()
    Wami.stopRecording()
    $(this).removeClass("fstop").addClass "frecording"
    $(".fplay,.fimport,.fdelete", $(this).parent()).removeClass "disabled"

  $(".fdelete").click (e) ->
    $(".fplay,.fimport,.fdelete", $(this).parent()).addClass "disabled"