class SessionSerializer
  def self.dump(x)
    Zlib.deflate(Marshal.dump(x), 9)
  end

  def self.load(x)
    return nil if x.nil?
    Marshal.load(Zlib.inflate(x)) rescue nil
  end
end