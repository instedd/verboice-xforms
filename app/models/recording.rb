module Recording
  def save_recording name, io
    dir = recordings_path
    FileUtils.mkpath dir

    filename = name + '.wav'
    File.open File.join(dir, filename), 'wb' do |f|
      IO.copy_stream io, f
    end
  end

  def get_recording name
    path = File.join recordings_path, name + '.wav'
    return path if File.exists?(path)
  end

  def delete_recording name
    path = get_recording name
    File.delete path unless path.nil?
  end
end