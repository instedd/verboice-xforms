class Endpoint < ActiveRecord::Base
  has_many :forms, :dependent => :destroy
  has_many :calls, :through => :forms

  validates_presence_of :name

  before_create :generate_guid

  include Recording

  def generate_guid
    self.guid ||= Digest::MD5.hexdigest(Time.now.to_f.to_s)
  end

  def post_results call
    if callback_url
      RestClient.post callback_url, {:xml_submission_file => call.data, :multipart => true}
    end
  end
  handle_asynchronously :post_results

  def with_time_zone
    old_time_zone = Time.zone
    Time.zone = time_zone
    yield
  ensure
    Time.zone = old_time_zone
  end

  def recordings_path
    File.join Rails.root, 'data', 'endpoint', self.id.to_s
  end
end
