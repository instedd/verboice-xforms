class Call < ActiveRecord::Base
  belongs_to :form
  has_one :endpoint, :through => :form

  before_create :save_values
  before_create :enqueue_in_verboice

  validates_presence_of :form_id
  validates_presence_of :phone

  attr_protected :endpoint

  serialize :session, SessionSerializer

  def enqueue_in_verboice
    result = Verboice.instance.call phone, verboice_options
    self.verboice_call_id = result['call_id']
    self.status = result['state']
  end

  def save_values
    if @values
      xml = form.xform.model_instance
      form.map_to_xml(xml, @values)
      self.data = xml.to_s
    end
  end

  def fields=(values)
    @values = values
  end

  def finish data
    update_attributes! :data => data
    endpoint.post_results self if endpoint
  end

  private

  def verboice_options
    opts = {:channel => endpoint.channel}
    opts[:schedule] = schedule if schedule
    opts[:not_before] = not_before if not_before
    opts
  end

  def schedule
    endpoint.try(:schedule)
  end
end
