class CallQueue
  
  def initialize(hash)
    @hash = hash
  end
  
  def method_missing(method, *args, &block)
    @hash[method]
  end
  
end