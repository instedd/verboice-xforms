class Form < ActiveRecord::Base
  belongs_to :endpoint
  has_many :calls, :dependent => :destroy

  serialize :mappings, Hash

  validates_presence_of :name
  validates_presence_of :endpoint
  validates_uniqueness_of :external_id, :scope => :endpoint_id

  include Recording

  def xform
    @xform ||= XForms::Form.parse xml
  end

  def map_from_xml(xml)
    xml = Nokogiri::XML(xml) if xml.is_a? String
    values = {}
    mappings.each do |name, xpath|
      values[name] = xml.xpath(xpath).first.try(:inner_text)
    end
    values
  end

  def map_to_xml(xml, values)
    mappings.each do |name, xpath|
      xml.xpath(xpath).first.try(:children=, values[name].to_s)
    end
  end

  def mappings_str
    mappings.to_json
  end

  def mappings_str=(value)
    self.mappings = JSON.parse value
  end

  def recordings_path
    File.join Rails.root, 'data', 'form', self.id.to_s
  end

end
