class EndpointsController < ApplicationController
  respond_to :html
  
  def edit
    @schedule = schedules.select{|q| q.name == endpoint.schedule}.first
  end

  def create
    endpoint.save
    respond_with endpoint, :location => endpoints_path
  end

  def update
    endpoint.save
    respond_with endpoint, :location => endpoints_path
  end

  def destroy
    endpoint.destroy
    respond_with endpoint
  end

  def enqueue
    endpoint = Endpoint.find_by_guid(params[:guid])
    
    form = endpoint.forms.where(:external_id => params[:form_id]).first

    data = form.xform.model_instance
    values = {}
    form.mappings.keys.each do |key|
      values[key] = params[key]
    end
    form.map_to_xml(data, values)
    
    time = endpoint.with_time_zone do
      Time.zone.parse("#{params[:call_date]} #{params[:call_time]}").try(:utc)
    end

    Call.create! :phone => params[:phone_number], :form => form, :data => data.to_s, :not_before => time
    render :text => endpoint.name
  end  
end