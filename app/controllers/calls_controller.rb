class CallsController < ApplicationController
  respond_to :html

  expose(:endpoint) { Endpoint.find params[:endpoint_id] if params[:endpoint_id].present?}  
  expose(:forms) { endpoint.try(:forms) }

  def create
    return render :new if request.xhr?
    call.save
    respond_with call, :location => calls_path
  end
end