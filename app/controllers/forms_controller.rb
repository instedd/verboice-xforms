class FormsController < ApplicationController
  respond_to :html
  
  before_filter :read_xml_file, :only => [:create, :update]

  def create
    if form.save
      flash[:notice] = "Form successfully created"
      redirect_to forms_path
    else
      respond_with form
    end
  end

  def update
    if form.save
      flash[:notice] = "Form successfully updated"
      redirect_to forms_path
    else
      respond_with form
    end
  end

  def destroy
    form.destroy
    flash[:notice] = "Form successfully deleted"
    respond_with form
  end
  
  def xml
    render :xml => form.xml
  end
  
  private
  
  def read_xml_file
    file = params[:form][:xml_file]
    if file
      params[:form].delete :xml_file
      form.xml = file.read
      form.file_name = file.original_filename
      form.file_date = Time.now.utc
    end
  end
  
end
