class ApplicationController < ActionController::Base
  protect_from_forgery

  expose(:forms) { Form.scoped }
  expose(:form)
  expose(:endpoints) { Endpoint.scoped }
  expose(:endpoint)
  expose(:calls) do
    if params[:endpoint_id].present?
      calls = Endpoint.find(params[:endpoint_id]).calls
    else
      calls = Call.scoped
      calls = calls.where(:form_id => params[:form_id]) if params[:form_id].present?
    end
    calls = calls.order('created_at DESC')
    calls.page(params[:page]).per(10)
  end
  expose(:call)
  expose(:schedules) { Verboice.instance.project_schedules.map{|q| CallQueue.new(q) } }
  expose(:channels) { Verboice.instance.list_channels }

end
