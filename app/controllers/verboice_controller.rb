require 'builder'

class VerboiceController < ApplicationController
  around_filter :verboice_session, :only => :callback
  around_filter :twiml_response, :only => :callback
  expose(:call) { Call.find_by_verboice_call_id(params[:CallSid]) }
  expose(:form) { call.form }

  def callback
    I18n.locale = :es
    if @session[:control]
      form.xform.model_instance = Nokogiri::XML(@session[:model_instance])
      control = form.xform.controls[@session[:control]]

      if control.readonly
        is_valid = true
      else
        is_valid = false
        if params[:Digits].present?
          pause
          if control.is_a? XForms::Input
            control.value = params[:Digits]
            is_valid = control.valid?
          elsif control.is_a? XForms::Select1
            index = params[:Digits].to_i - 1
            if index < control.items.length
              control.value = control.items[index].value
              is_valid = true
            end
          end
        end

        if is_valid && form.endpoint.get_recording('confirmation_message')
          play endpoint_recording_url(form.endpoint, 'confirmation_message')
        end
      end

      if is_valid
        @session[:control] += 1
      else
        say control.constraint_message if control.constraint_message.present?
        @session[:retry] = (@session[:retry] || 0) + 1

        if @session[:retry] >= form.endpoint.retry_count
          play endpoint_recording_url(form.endpoint, 'error_message') if form.endpoint.get_recording('error_message')
          return hangup
        end
      end
    else
      @session[:control] = 0
      form.xform.model_instance = Nokogiri::XML(call.data) if call.data.present?
    end

    @session[:control] += 1 while control_irrelevant?
    @session[:model_instance] = form.xform.model_instance.to_s

    if @session[:control] < form.xform.controls.count
      control = form.xform.controls[@session[:control]]
      if control.is_a? XForms::Input
        if control.readonly
          say control.label
          say control.value
        else
          gather :say => control.label
        end
      elsif control.is_a? XForms::Select1
        gather :say => control.label, :numDigits => 1, :timeout => 0
        control.items.each.with_index do |item, i|
          if i < control.items.count - 1
            gather :say => I18n.t(:press_number_for_text, :number => i + 1, :text => item.label), :numDigits => 1, :timeout => 0
          else
            gather :say => I18n.t(:press_number_for_text, :number => i + 1, :text => item.label), :numDigits => 1
          end
        end
      end
      redirect callback_url
    else
      call.finish @session[:model_instance]
      hangup
    end
  end

  def status
    call.update_attributes! :status => params[:CallStatus]
    render :text => nil
  end

  private

  def control_irrelevant?
    @session[:control] < form.xform.controls.count && !form.xform.controls[@session[:control]].relevant?
  end

  def twiml_response
    @xml = Builder::XmlMarkup.new
    @xml.Response do
      send(params[:action])
    end
    render :xml => @xml.target!
  end

  def verboice_session
    @session = call.session || {}
    yield
    call.update_attributes! :session => @session
  end

  def answer
    @xml.Answer
  end

  def hangup
    @xml.Hangup
  end

  def play url
    @xml.Play(url)
  end

  def say text
    if text.present?
      if form.get_recording(text.md5)
        play form_recording_url(form, text.md5)
      else
        @xml.Say(text)
      end
    end
  end

  def pause
    @xml.Pause
  end

  def gather options
    if options[:say] && form.get_recording(options[:say].md5)
      options[:play] = form_recording_url(form, options[:say].md5)
      options.delete :say
    end

    @xml.Gather(options.slice(:action, :numDigits, :timeout)) do
      @xml.Play options[:play] if options[:play]
      @xml.Say options[:say] if options[:say]
    end
  end

  def redirect url
    @xml.Redirect(url)
  end
end