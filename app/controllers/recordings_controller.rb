class RecordingsController < ApplicationController

  def show
    send_file entity.get_recording(params[:id]), :filename => params[:filename]
  end

  def save
    entity.save_recording params[:id], input_file
    render :text => nil
  end

  def destroy
    entity.delete_recording params[:id]
    render :text => nil
  end

  private

  def input_file
    return params[:files][0] if params[:files]
    request.body
  end

end