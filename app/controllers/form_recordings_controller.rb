class FormRecordingsController < RecordingsController

  def index
    I18n.locale = :es
    @strings = []
    form.xform.controls.each do |control|
      @strings << control.label if control.label.present?
      @strings << control.constraint_message if control.constraint_message.present?
      @strings << control.value.to_s if control.readonly && control.value.present?

      if control.is_a? XForms::Select1
        control.items.each_with_index do |item, i|
          @strings << I18n.t(:press_number_for_text, :number => i + 1, :text => item.label)
        end
      end
    end
  end

  def entity
    form
  end

end